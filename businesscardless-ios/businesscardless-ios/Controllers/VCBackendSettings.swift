//
//  VCBackendSettings.swift
//  businesscardless-ios
//
//  Created by Александр on 8/6/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import UIKit

class VCBackendSettings: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var backendUrlTextField: UITextField!
    
    override func viewDidLoad() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidLoad()

        backendUrlTextField.delegate = self
        backendUrlTextField.text = Defaults.backendUrl
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }

    @IBAction func saveBackendUrlButtonClick(sender: AnyObject) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): saving: \(backendUrlTextField.text!) value to UserDefaults")
        
        Defaults.backendUrl = backendUrlTextField.text!
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
        
    override func didReceiveMemoryWarning() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
