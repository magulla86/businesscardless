//
//  VCPersonsAroundList+TableViewController.swift
//  businesscardless-ios
//
//  Created by Александр on 8/16/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import UIKit

extension VCPersonsAroundList {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personsAroundList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Person Around Cell", forIndexPath: indexPath) as UITableViewCell
        
        let personAround = personsAroundList[indexPath.row]
        
        cell.textLabel?.text = "\(personAround.userName)"
        cell.detailTextLabel?.text = "\(personAround.mM.major):\(personAround.mM.minor), \(personAround.userId), \(ProximityListStringValues.getStringValueForProximity(personAround.proximity))"
        cell.imageView?.image = personAround.userImage
        
        if (personAround.isStored == true) {
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        } else {
            if (personAround.isSelected == true) {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.None
            }
        }
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if personsAroundList[indexPath.row].isSelected == true {
           personsAroundList[indexPath.row].isSelected = false
        } else {
            personsAroundList[indexPath.row].isSelected = true
        }
        tableView.reloadData()
    }
}