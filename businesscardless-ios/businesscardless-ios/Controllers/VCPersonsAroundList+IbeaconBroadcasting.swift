//
//  VCPersonsAroundList+IbeaconBroadcasting.swift
//  businesscardless-ios
//
//  Created by Александр on 8/4/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreLocation

extension VCPersonsAroundList : CBPeripheralManagerDelegate {
    
    @IBAction func startBroadcastButtonClick(sender: AnyObject) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        if !isBroadcasting {
            startBroadcast(self.majorMinorForBroadcast!)
        } else {
            stopBroadcast()
        }
    }
    
    func startBroadcast(mm: MajorMinorStruct) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        if isBroadcasting == false {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): isBroadcasting is false, trying to enable")
            if bluetoothPeripheralManager.state == CBPeripheralManagerState.PoweredOn {
                let major: CLBeaconMajorValue = UInt16(mm.major)
                let minor: CLBeaconMinorValue = UInt16(mm.minor)
                beaconRegionForBroadcast = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: Defaults.applicationUUID)!, major: major, minor: minor, identifier: "com.appcoda.beacondemo")
                let dataDictionary = beaconRegionForBroadcast.peripheralDataWithMeasuredPower(nil)
                bluetoothPeripheralManager.startAdvertising(dataDictionary as? [String : AnyObject])
                startBroadcastButton.title = "Stop"
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Broadcasting... ")
                isBroadcasting = true
            } else {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): bluetoothPeripheralManager.state is not CBPeripheralManagerState.PoweredOn")
            }
        } else {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): isBroadcasting is true")
        }
    }
    
    func stopBroadcast() {
        if isBroadcasting {
            bluetoothPeripheralManager.stopAdvertising()
            startBroadcastButton.title = "Start"
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Stopped broadcasting")
            isBroadcasting = false
        }
    }
    
    func getBEMMForUserId (userId : String, userName : String) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        let urlString = Defaults.prepareServiceUrlGetMMForUserId(userId, userName: userName)
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            let newdata = Helper.backendRequestToFetchData(urlString)
            if (newdata != nil) {
                let major = ((newdata!["mm"] as! NSDictionary)["major"]) as! Int
                let minor = ((newdata!["mm"] as! NSDictionary)["minor"]) as! Int
                self.majorMinorForBroadcast = MajorMinorStruct(major: major, minor: minor)
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Reading from parsed json NSDictionary major: \(major) and minor: \(minor)")
                dispatch_async(dispatch_get_main_queue()) {
                    self.checkUserLoginStatus()
                }
            } else {
                Helper().alertMessage("Error with backendcall: \(urlString)", uiViewController: self)
            }
        }
    }
    
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        var statusMessage = ""
        
        switch peripheral.state {
        case CBPeripheralManagerState.PoweredOn:
            statusMessage = "Bluetooth Status: Turned On"
            
        case CBPeripheralManagerState.PoweredOff:
            if isBroadcasting {
                startBroadcastButtonClick(self)
            }
            statusMessage = "Bluetooth Status: Turned Off"
            
        case CBPeripheralManagerState.Resetting:
            statusMessage = "Bluetooth Status: Resetting"
            
        case CBPeripheralManagerState.Unauthorized:
            statusMessage = "Bluetooth Status: Not Authorized"
            
        case CBPeripheralManagerState.Unsupported:
            statusMessage = "Bluetooth Status: Not Supported"
            
        default:
            statusMessage = "Bluetooth Status: Unknown"
        }
        
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): \(statusMessage)")
    }
}