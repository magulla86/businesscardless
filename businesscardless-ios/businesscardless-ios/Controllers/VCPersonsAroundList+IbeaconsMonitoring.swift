//
//  VCPersonsAroundList+IbeaconsMonitoring.swift
//  businesscardless-ios
//
//  Created by Александр on 8/4/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import CoreLocation
import CoreBluetooth
import UIKit


extension VCPersonsAroundList: CLLocationManagerDelegate {
    
    func startSearchForIBeacons () {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        locationManagerForMonitoring = CLLocationManager()
        if(locationManagerForMonitoring!.respondsToSelector("requestAlwaysAuthorization")) {
            locationManagerForMonitoring!.requestAlwaysAuthorization()
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): requested requestAlwaysAuthorization")
        }
        
        locationManagerForMonitoring!.delegate = self
        locationManagerForMonitoring!.pausesLocationUpdatesAutomatically = false
        
        locationManagerForMonitoring!.startMonitoringForRegion(beaconForRegionMonitoring)
        locationManagerForMonitoring!.startRangingBeaconsInRegion(beaconForRegionMonitoring)
        locationManagerForMonitoring!.startUpdatingLocation()
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): started \(__FUNCTION__)")
        
        let application : UIApplication = UIApplication.sharedApplication()
        
        if(application.respondsToSelector("registerUserNotificationSettings:")) {
            application.registerUserNotificationSettings(
                UIUserNotificationSettings(
                    forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Sound],
                    categories: nil
                )
            )
        }
        addIBeaconTestData()
    }
    
    func stopSearchForIBeacons () {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        if locationManagerForMonitoring  != nil {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): VCPersonsAroundList.locationManagerForMonitoring is Not nil, initiating \(__FUNCTION__)")
            locationManagerForMonitoring.stopMonitoringForRegion(beaconForRegionMonitoring)
            locationManagerForMonitoring.stopRangingBeaconsInRegion(beaconForRegionMonitoring)
        }
    }
    
    func locationManager(manager: CLLocationManager,
        didRangeBeacons beacons: [CLBeacon],
        inRegion region: CLBeaconRegion) {
//            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
            
            var personsAroundTMPList = [PersonAround]()
            
            if(beacons.count > 0) {
                for beacon : CLBeacon in beacons {
                    let majorMinorForBeaconAround = MajorMinorStruct(major: Int(beacon.major), minor: Int(beacon.minor))
                    
                    //execute call only if this mm is not present in the list
                    if let userIdForMajorMinorSavedInArray = knownMajorMinorAroundArray[majorMinorForBeaconAround] {
                        // NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): backend was already requested for this MajorMinor and it's saved to array: \(majorMinorForBeaconAround)")
                        let personAround = PersonAround(userId: userIdForMajorMinorSavedInArray.userId, userName: userIdForMajorMinorSavedInArray.userName, image: userIdForMajorMinorSavedInArray.userImage, mM: majorMinorForBeaconAround, proximity: beacon.proximity, isSelected: true, isStored: false)
                        personsAroundTMPList.append(personAround)
                    } else {
                        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): This MajorMinor was not saved, requesting backend and saving: \(majorMinorForBeaconAround)")
                        

                        //TODO-BUG: here iOS manages to request backend 3 times before get's first response
                        let urlString = Defaults.prepareServiceUrlGetUserDetailsForMM(beacon.major as Int, minor: beacon.minor as Int)
                        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
                            let newdata = Helper.backendRequestToFetchData(urlString)
                            if (newdata != nil) {
                                let userId = newdata!["userId"] as! String
                                let userName = newdata!["userName"] as! String
                                let profilePictureSmallImage = Helper.backendRequestToFetchImage(userId)
                                let personAround = PersonAround(userId: userId, userName: userName, image: profilePictureSmallImage!, mM: majorMinorForBeaconAround, proximity: beacon.proximity, isSelected: true, isStored: false)
                                self.knownMajorMinorAroundArray[majorMinorForBeaconAround] = personAround
                                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Reading from parsed json NSDictionary userId: \(userId), userName: \(userName) and adding it to list knownMajorMinorAroundArray")
                            } else {
                                Helper().alertMessage("Error with backendcall: \(urlString)", uiViewController: self)
                                let image = UIImage (named: "no-image")
                                let personAround = PersonAround(userId: "N/A. \(majorMinorForBeaconAround.major):\(majorMinorForBeaconAround.minor)", userName: "CheckNetwork", image: image!, mM: MajorMinorStruct(major: -1, minor: -1), proximity: CLProximity.Unknown, isSelected: false, isStored: false)
                                self.knownMajorMinorAroundArray[majorMinorForBeaconAround] = personAround
                            }
                        }
                    }
//                    if(beacon.proximity == CLProximity.Unknown) {
//                        return;
//                    }
                }
            }
            
            personsAroundList = personsAroundTMPList
            
            tableView.reloadData()
    }
    
    func locationManager(manager: CLLocationManager,
        didEnterRegion region: CLRegion) {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
            manager.startRangingBeaconsInRegion(region as! CLBeaconRegion)
            manager.startUpdatingLocation()
            
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): You entered the region")
            Helper().sendLocalNotificationWithMessage("You entered the region \(region)")
    }
    
    func locationManager(manager: CLLocationManager,
        didExitRegion region: CLRegion) {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
            manager.stopRangingBeaconsInRegion(region as! CLBeaconRegion)
            manager.stopUpdatingLocation()
            
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): You exited the region")
            Helper().sendLocalNotificationWithMessage("You exited the region \(region)")
    }
    
    func addIBeaconTestData () {
        var testPA = [PersonAround]()
//{"userId":"1501186993505130","userName":"Jack String","mm":{"major":0,"minor":9,"sequence":9}},
//{"userId":"949641135100119","userName":"Alexander Kovalenko","mm":{"major":0,"minor":10,"sequence":10}},
//{"userId":"951723991545962","userName":"Oleksandr Pochapskyy","mm":{"major":0,"minor":11,"sequence":11}},
//{"userId":"10206747559514803","userName":"Znosheni Kedy","mm":{"major":0,"minor":13,"sequence":13}}]
        let image1 = Helper.backendRequestToFetchImage("1501186993505130")
        let person1 = PersonAround(userId: "1501186993505130", userName: "Jack String", image: image1!, mM: MajorMinorStruct(major: 0, minor: 9), proximity: CLProximity.Unknown, isSelected: true, isStored: false)
        let image2 = Helper.backendRequestToFetchImage("949641135100119")
        let person2 = PersonAround(userId: "949641135100119", userName: "Alexander Kovalenko", image: image2!, mM: MajorMinorStruct(major: 0, minor: 10), proximity: CLProximity.Unknown, isSelected: true, isStored: false)
        let image3 = Helper.backendRequestToFetchImage("951723991545962")
        let person3 = PersonAround(userId: "951723991545962", userName: "Oleksandr Pochapskyy", image: image3!, mM: MajorMinorStruct(major: 0, minor: 11), proximity: CLProximity.Unknown, isSelected: true, isStored: false)
        let image4 = Helper.backendRequestToFetchImage("10206747559514803")
        let person4 = PersonAround(userId: "10206747559514803", userName: "Znosheni Kedy", image: image4!, mM: MajorMinorStruct(major: 0, minor: 13), proximity: CLProximity.Unknown, isSelected: true, isStored: false)

        testPA.append(person1)
        testPA.append(person2)
        testPA.append(person3)
        testPA.append(person4)
        
        personsAroundList = testPA
    }
}
