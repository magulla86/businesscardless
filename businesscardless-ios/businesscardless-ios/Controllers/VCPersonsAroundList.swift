//
//  VCPersonsAroundList.swift
//  businesscardless-ios
//
//  Created by Александр on 7/12/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import CoreLocation
import FBSDKCoreKit
import CoreBluetooth


class VCPersonsAroundList : UITableViewController, UIApplicationDelegate {
    
    //PROPERTIES for search of ibeacons around
    var locationManagerForMonitoring : CLLocationManager!
    let beaconForRegionMonitoring = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: Defaults.applicationUUID)!, identifier: Defaults.beaconIdentifier)
    var personsAroundList = [PersonAround]() //actual list of persons around based on ibeacons around
    var knownMajorMinorAroundArray = [MajorMinorStruct : PersonAround]() //a list of persons around where we have requested details for user id. This list is not similar to list above as list of beacons around changes constantly
    
    //OUTLETS
    @IBOutlet weak var userDetailsButton: UIBarButtonItem!
    @IBOutlet weak var startBroadcastButton: UIBarButtonItem!
    
    //PROPERTIES for ibeacon advertisement
    var majorMinorForBroadcast : MajorMinorStruct!
    var beaconRegionForBroadcast: CLBeaconRegion!
    var bluetoothPeripheralManager: CBPeripheralManager!
    var isBroadcasting = false
    
    override func viewDidLoad() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidLoad()
        
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
        
        //Subscribing to notifications from AppDelegate to receive enter_background/become_active events
        let mainQueue = NSOperationQueue.mainQueue()
        let appDelegate = UIApplication.sharedApplication().delegate
        Defaults.notificationCenter.addObserverForName(AppBackForegroundNotification.name, object: appDelegate, queue: mainQueue) { notification in
            if let appState = notification.userInfo?[AppBackForegroundNotification.keyForState] as? String {
                if  Defaults.settings.objectForInfoDictionaryKey(Defaults.keyForApplicationMonitoringIsDeactivatedOnBackground) as! Bool  == true {
                    NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Plist ApplicationMonitoringIsDeactivatedOnBackground is True")
                    if appState == AppBackForegroundNotification.valueForBackground {
                        self.stopSearchForIBeacons()
                        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle - Received notification: \(appState)")
                    } else if appState == AppBackForegroundNotification.valueForForeground {
                        self.startSearchForIBeacons()
                        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle - Received notification: \(appState)")
                    } else {
                        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle - Error: received unknown notification: \(appState)")
                    }
                } else {
                    NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Plist ApplicationMonitoringIsDeactivatedOnBackground is False")
                }
            }
        }
        
        startSearchForIBeacons()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        checkUserLoginStatus()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }
    
    
    func checkUserLoginStatus() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): checking if FBSDKProfile.currentProfile is loaded")
        let fbProfile : FBSDKProfile! = FBSDKProfile.currentProfile()
        if fbProfile != nil {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): FBSDKProfile.currentProfile is Not nil. FBSDKProfile.currentProfile: \(fbProfile)")
            
            userDetailsButton.title = "\(fbProfile.name)"
            
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): checking if majorMinorForBroadcast is loaded")
            if majorMinorForBroadcast == nil {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): majorMinorForBroadcast is Nil")
            
                getBEMMForUserId(fbProfile.userID as String, userName: fbProfile.name as String)
            } else {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): majorMinorForBroadcast is Not nil: \(majorMinorForBroadcast), enabling startBroadcastButton button")
                startBroadcastButton.enabled = true
                startBroadcast(self.majorMinorForBroadcast!)
            }
        } else {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): FBSDKProfile.currentProfile is Nil, disabling startBroadcastButton")
            userDetailsButton.title = "Login"
            startBroadcastButton.enabled = false
        }
    }
}
