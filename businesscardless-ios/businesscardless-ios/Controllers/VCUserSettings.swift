//
//  VCMainUserView.swift
//  businesscardless-ios
//
//  Created by Александр on 7/9/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import FBSDKCoreKit

class VCUserSettings : UIViewController, NSURLConnectionDelegate {
    
    @IBOutlet weak var loginWithFacebookButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    var counterValue = 0
    
    override func viewDidLoad() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidLoad()
        
        checkUserLoggedInAndUpdateLabel()
    }
    
    override func viewDidAppear(animated: Bool) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidAppear(animated)
        
        checkUserLoggedInAndUpdateLabel()
    }
    
    override func viewDidDisappear(animated: Bool) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidDisappear(animated)
    }
    
    func checkUserLoggedInAndUpdateLabel () {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        if (FBSDKAccessToken.currentAccessToken() != nil)
        {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): FBSDKAccessToken.currentAccessToken() is Not nil")
            
            //user is logged in
            let fbProfile : FBSDKProfile! = FBSDKProfile.currentProfile()
            if fbProfile != nil {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): FBSDKProfile.currentProfile() is Not nil")
                statusLabel.text = "Signed \(fbProfile.name), \(fbProfile.userID)"
            }
        }
        else
        {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): FBSDKAccessToken.currentAccessToken() is Nil")
            
            statusLabel.text = "Not signed in"
        }
    }
    
}
