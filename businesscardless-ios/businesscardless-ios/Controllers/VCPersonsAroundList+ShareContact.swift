//
//  VCPersonsAroundList+ShareContact.swift
//  businesscardless-ios
//
//  Created by Александр on 8/16/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import UIKit
import FBSDKCoreKit

extension VCPersonsAroundList {
    
    @IBAction func shareContactButtonClick(sender: AnyObject) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        initiateShareContactRequest(personsAroundList)
    }
    
    func initiateShareContactRequest (personsAroundToShareContact: [PersonAround]) {
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) { () -> Void in
            for pa : PersonAround in personsAroundToShareContact {
                let fbProfile : FBSDKProfile! = FBSDKProfile.currentProfile()
                let urlString = Defaults.prepareServiceUrlRequestContactSharing(fbProfile.userID, personAroundUserId: pa.userId)
                
                let backendCallResultData = Helper.backendRequestToFetchData(urlString)
                if (backendCallResultData != nil) {
                    //TODO: change according to BE format
                    let backendCallResult = backendCallResultData!["result"] as! NSDictionary
                    NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Reading from parsed json NSDictionary result: \(backendCallResult)")
                } else {
                    Helper().alertMessage("Error with backendcall: \(urlString)", uiViewController: self)
                }
            }
        }
    }
}