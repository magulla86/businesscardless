//
//  VCFacebookLogin.swift
//  businesscardless-ios
//
//  Created by Александр on 7/8/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class VCFacebookLogin : UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var profilePictureView: FBSDKProfilePictureView!
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    
    override func viewDidLoad() {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        super.viewDidLoad()
        
        if (FBSDKAccessToken.currentAccessToken() != nil)
        {
            // User is already logged in, do work such as go to next view controller.
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
            graphRequest.startWithCompletionHandler({
                (connection, result, error) -> Void in
                if ((error) != nil)
                {
                    // Process error
                    NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): error happened during  FBSDKGraphRequest(graphPath: \"me\": \(error)")
                }
                else
                {
                    let userName : NSString = result.valueForKey("name") as! NSString
                    let userEmail : NSString = result.valueForKey("email") as! NSString
                    let userID : NSString = result.valueForKey("id") as! NSString
                
                    NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): fetched user: \(result), userName: \(userName), userEmail: \(userEmail), userId: \(userID)")
                    
                    let currentUserProfile : FBSDKProfile! = FBSDKProfile.currentProfile();
                    if currentUserProfile != nil {
                        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): currentUserProfile is loaded via FBSDKProfile.currentProfile(): \(currentUserProfile)")
                    }  else {
                        NSLog ("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): currentUserProfile is nil")
                    }
                }
            })
        }
        else
        {
            facebookLoginButton.readPermissions = ["public_profile", "email", "user_friends"]
            facebookLoginButton.delegate = self
            FBSDKProfile.enableUpdatesOnAccessTokenChange(true)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "onProfileUpdated:", name:FBSDKProfileDidChangeNotification, object: nil)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }
    
    func onProfileUpdated(notification: NSNotification)
    {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
    }

    
    // Facebook Delegate Methods
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
            }
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
    }
}
