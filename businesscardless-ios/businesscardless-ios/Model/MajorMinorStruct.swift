//
//  MajorMinorStruct.swift
//  businesscardless-ios
//
//  Created by Александр on 8/1/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

class MajorMinorStruct : Hashable, CustomStringConvertible {
    
    var major : Int
    var minor : Int
    
    init (major : Int, minor : Int) {
        self.major = major
        self.minor = minor
    }
    
    var hashValue : Int {
        get {
            return "\(self.major)_\(self.minor)".hashValue
        }
    }
    
    var description : String {
        return "[major: \(major), minor: \(minor)]"
    }
}

func ==(majorMinor1 : MajorMinorStruct, majorMinor2 : MajorMinorStruct) -> Bool {
    return majorMinor1.hashValue == majorMinor2.hashValue
}