//
//  PersonAround.swift
//  businesscardless-ios
//
//  Created by Александр on 7/11/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class PersonAround : CustomStringConvertible {
    
    var userId : String
    var userName : String
    var userImage : UIImage
    var mM : MajorMinorStruct
    var proximity: CLProximity
    var isSelected : Bool
    var isStored : Bool
    
    init (userId : String, userName : String, image : UIImage, mM : MajorMinorStruct, proximity : CLProximity, isSelected: Bool, isStored : Bool) {
        self.userId = userId
        self.userName = userName
        self.userImage = image
        self.mM = mM
        self.proximity = proximity
        self.isSelected = isSelected
        self.isStored = isStored
    }
    
    var description : String {
        return "userId: \(userId), major: \(mM.major), minor: \(mM.minor), userName: \(userName), userImage: \(userImage), proximity: \(proximity), isSelected: \(isSelected), isStored: \(isStored)."
    }
}