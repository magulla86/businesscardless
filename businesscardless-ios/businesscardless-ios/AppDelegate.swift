//
//  AppDelegate.swift
//  businesscardless-ios
//
//  Created by Oleksandr Pochapskyy on 7/7/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import CoreLocation


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window : UIWindow!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func application(application: UIApplication,
        openURL url: NSURL,
        sourceApplication: String?,
        annotation: AnyObject?) -> Bool {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
            return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
        
        FBSDKAppEvents.activateApp()
        
        let notification = NSNotification(name: AppBackForegroundNotification.name, object: self, userInfo: [AppBackForegroundNotification.keyForState:AppBackForegroundNotification.valueForForeground])
        Defaults.notificationCenter.postNotification(notification)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle - posted notification")

    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
       
        let notification = NSNotification(name: AppBackForegroundNotification.name, object: self, userInfo: [AppBackForegroundNotification.keyForState:AppBackForegroundNotification.valueForBackground])
        Defaults.notificationCenter.postNotification(notification)
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle - posted notification")
    }
    
    func applicationWillTerminate(application: UIApplication) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Lifecycle init")
    }
}