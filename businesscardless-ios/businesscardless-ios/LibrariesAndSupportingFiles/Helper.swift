//
//  Helper.swift
//  businesscardless-ios
//
//  Created by Александр on 8/6/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import UIKit

class Helper : UIViewController {
    func alertMessage (message : String, uiViewController : UIViewController, title : String = "Something went wrong") {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) -> Void in  }))
        
        
        dispatch_async(dispatch_get_main_queue()) {
            uiViewController.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func sendLocalNotificationWithMessage(message: String!) {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        let notification:UILocalNotification = UILocalNotification()
        notification.alertBody = message
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    static func escapeStringForUrlCall (urlStringNoEscape : String) -> String {
        return urlStringNoEscape.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    static func backendRequestToFetchImage (userId : String) -> UIImage? {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        var image : UIImage?
        
        let url = NSURL(string: "http://graph.facebook.com/\(userId)/picture")
        if let data = NSData(contentsOfURL: url!)
        {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): managed to download image from url: \(url)")
            image = UIImage(data: data)!
        }
        return image
    }
    
    static func backendRequestToFetchData (urlString: String) -> NSDictionary? {
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
        
        let url = NSURL (string: urlString)
        let data = NSData(contentsOfURL: url!)
        var result : NSDictionary?
        NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): trying to execute backend request: \(url)")
        if (data != nil) {
            let contents = NSString(data:data!, encoding:NSUTF8StringEncoding)
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): succesfuly executed backend request, loaded json data: \(contents!)")
            do {
                result = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Parsing loaded json data into NSDictionary: \(result)")
                return result
            } catch {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Error during parsing of loaded json data into NSDictionary")
                print(error)
                return result
            }
        } else {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): Error during execute of backend request: \(url)")
            return result
        }
    }
}