//
//  Defaults.swift
//  businesscardless-ios
//
//  Created by Александр on 8/13/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import UIKit

class Defaults {
    
    static let defaults = NSUserDefaults.standardUserDefaults()
    static let settings = NSBundle.mainBundle()
    static let notificationCenter = NSNotificationCenter.defaultCenter()
    
    static let keyForBackendUrl = "BackendUrl"
    static let keyForApplicationUUID = "ApplicationUUID"
    static let keyForBeaconIdentifier = "BeaconIdentifier"
    static let keyForApplicationMonitoringIsDeactivatedOnBackground = "ApplicationMonitoringIsDeactivatedOnBackground"

    static let applicationUUID = Defaults.settings.objectForInfoDictionaryKey(Defaults.keyForApplicationUUID) as! String
    static let beaconIdentifier = Defaults.settings.objectForInfoDictionaryKey(Defaults.keyForBeaconIdentifier) as! String
    
    static var backendUrl : String {
        get {
            NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): init")
            //check if possible to read backendurl from NSUserDefaults
            if let backendUrlFromNSUserDefaults = Defaults.defaults.stringForKey(Defaults.keyForBackendUrl) {
                //if possible to read then return use it
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): managed to read from userDefaults: \(backendUrlFromNSUserDefaults)")
                return backendUrlFromNSUserDefaults
            } else {
                NSLog("\(_stdlib_getDemangledTypeName(self)).\(__FUNCTION__): reading from info.plist: \(Defaults.settings.objectForInfoDictionaryKey(Defaults.keyForBackendUrl))")
                //if not possible to read, load from info.plist
                return (Defaults.settings.objectForInfoDictionaryKey(Defaults.keyForBackendUrl) as? String)!
            }
        }
        set {
            Defaults.defaults.setObject(newValue, forKey: Defaults.keyForBackendUrl)
        }
    }
    
    static func prepareServiceUrlGetMMForUserId (userId : String, userName : String) -> String {
        return Helper.escapeStringForUrlCall(String(format: "\(Defaults.backendUrl)/getMMForUserId?userId=%@&userName=%@", userId, userName))
    }
    
    static func prepareServiceUrlGetUserDetailsForMM (major : Int, minor : Int) -> String {
        return Helper.escapeStringForUrlCall(String(format: "\(Defaults.backendUrl)/getUserDetailsForMM?major=%d&minor=%d", major, minor))
    }
    
    static func prepareServiceUrlRequestContactSharing (currentUserId : String, personAroundUserId : String) -> String {
        return Helper.escapeStringForUrlCall(String(format: "\(Defaults.backendUrl)/makeSharingrequst?currentUserId=%@&personAroundUserId=%@", currentUserId, personAroundUserId))
    }
}

struct AppBackForegroundNotification {
    static let name = "applicationBackgroudForegroundChangeNotification"
    static let keyForState = "State"
    static let valueForForeground = "Foreground"
    static let valueForBackground = "Background"
}
