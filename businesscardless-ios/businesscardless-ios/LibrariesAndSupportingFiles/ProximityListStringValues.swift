//
//  ProximityListStringValues.swift
//  businesscardless-ios
//
//  Created by Александр on 7/12/15.
//  Copyright © 2015 Oleksandr Pochapskyy. All rights reserved.
//

import Foundation
import CoreLocation

class ProximityListStringValues {
    
    static func getStringValueForProximity (proximity: CLProximity) -> String {
        
        switch proximity {
        case CLProximity.Far:
            return "Far"
        case CLProximity.Near:
            return "Near"
        case CLProximity.Immediate:
            return "Immediate"
        case CLProximity.Unknown:
            return "Unknown"
        }
    }
    
}