package com.reckless.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

@org.springframework.context.annotation.Configuration
@PropertySource("classpath:application.properties")
public class SpringMongoConfig extends AbstractMongoConfiguration {

    @Autowired
    Environment environment;

    @Override
    public String getDatabaseName() {
        return environment.getProperty("mongodb.db");
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(
                environment.getProperty("mongodb.host"),
                Integer.valueOf(environment.getProperty("mongodb.port"))
        );
    }
}