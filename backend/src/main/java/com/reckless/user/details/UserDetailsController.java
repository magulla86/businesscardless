package com.reckless.user.details;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reckless.ibeacon.IBeaconData;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

@RestController
public class UserDetailsController {

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/userDetails", method = RequestMethod.GET)
	@Cacheable
	public UserDetails getUserDetailsForIBeaconMajorAndMinor(@RequestParam(value = "major") int major, @RequestParam(value="minor")int minor) {
		return userDetailsService.findUserDetailsForUserWithMajorAndMinor(major, minor);
	}
	@RequestMapping(value = "/userDetails-batch", method = RequestMethod.GET)
	public List< UserDetails> getUserDetailsForIBeaconData(@RequestParam(value = "ibecons") List <IBeaconData> ibeacons) {
		for (IBeaconData iBeaconData : ibeacons) {
			getUserDetailsForIBeaconMajorAndMinor(iBeaconData.major, iBeaconData.minor);
		}
		return java.util.Collections.emptyList();//TODO ;
	}
}
