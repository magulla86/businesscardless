package com.reckless.user.details;

public class UserAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final String string;

	public UserAlreadyExistException(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
