package com.reckless.user.details;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reckless.persistence.UserDocument;
import com.reckless.persistence.UserDocumentRepository;

@Service
public class UserDetailsService {


    @Autowired
    private UserDocumentRepository userDocumentRepository;

    public UserDetails findUserDetailsForUserWithMajorAndMinor(int major, int minor) {
        UserDocument userDocument = userDocumentRepository.findByMajorAndMinor(major,minor);
        return userDocument.getUserDetails();
    }
}
