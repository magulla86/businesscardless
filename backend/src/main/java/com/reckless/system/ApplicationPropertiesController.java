package com.reckless.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationPropertiesController {

	@Autowired
	Environment environment;

	@RequestMapping(value = "/applicationProperty", method = RequestMethod.GET)
	public String getApplicationProperty(@RequestParam(value="propertyName")String propertyName) {
		return environment.getProperty(propertyName);
	}

}
