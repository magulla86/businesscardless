package com.reckless.ibeacon;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Random;

public class IBeaconData implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final int LIMIT = 65535;

    public final int major;
    public final int minor;

    @Id
    public final String userId;

    // TODO: Replace with proper database generation.
    // READ: http://www.mkyong.com/mongodb/spring-data-mongodb-auto-sequence-id-example/
    public static IBeaconData withRandomMajorMinorPair(String userId) {
        Random random = new Random();
        return new IBeaconData(random.nextInt(LIMIT), random.nextInt(LIMIT), userId);
    }
    public static IBeaconData withMajorMinorPair(Integer major, Integer minor,String userId) {
        return new IBeaconData( major,minor, userId);
    }
    public IBeaconData(int major, int minor, String userId) {
        this.major = major;
        this.userId = userId;
        this.minor = minor;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + major;
        result = prime * result + minor;
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IBeaconData other = (IBeaconData) obj;
        if (major != other.major)
            return false;
        if (minor != other.minor)
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

}
