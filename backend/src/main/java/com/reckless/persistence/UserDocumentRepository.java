package com.reckless.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserDocumentRepository extends MongoRepository<UserDocument, String> {

    UserDocument findByUserId(String userId);

    @Query("{ 'account.iBeaconData.major' : ?0, 'account.iBeaconData.minor' : ?1 }")
    UserDocument findByMajorAndMinor(int major, int minor);
}
