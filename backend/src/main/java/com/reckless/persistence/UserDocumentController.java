package com.reckless.persistence;

import com.reckless.account.Account;
import com.reckless.user.details.UserAlreadyExistException;
import com.reckless.user.details.UserDetails;
import com.reckless.ibeacon.IBeaconData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserDocumentController {

	@Autowired
	private UserDocumentRepository userDocumentRepository;

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public IBeaconData registerNewUserAndGetIbeacon(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "userName") String userName,
			@RequestParam(value = "firstName") String firstName,
			@RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "email") String email) {
		return registerUser(userId, userName, firstName, lastName, email).account.iBeaconData;
	}

	private UserDocument registerUser(String userId, String userName,
			String firstName, String lastName, String email) {
		if (userDocumentRepository.exists(userId)) {
			throw new UserAlreadyExistException("User with id '" + userId
					+ "' already exists");
		} else {
			UserDocument userDocument = new UserDocument(userId, new Account(
					userName, IBeaconData.withRandomMajorMinorPair(userId)),
					new UserDetails(firstName, lastName, email));
			return userDocumentRepository.insert(userDocument);
		}
	}



}
