package com.reckless.persistence;

import static org.junit.Assert.*;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.reckless.Application;
import com.reckless.account.Account;
import com.reckless.ibeacon.IBeaconData;
import com.reckless.user.details.UserDetails;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UserDocumentRepositoryTest {

	
	@Autowired
		private UserDocumentRepository userDocumentRepository;


	@Test
	public void createAndSaveNewUserDocument() {
		UserDocument userDocument = createNewUserDocument();
		UserDocument savedObject = userDocumentRepository.save(userDocument);
		Assertions.assertThat(savedObject).isNotNull();
	}
	@Test
	public void findUserDocumentByMajorAndMinor() {
		UserDocument userDocument = createNewUserDocument();
		UserDocument foundObject = userDocumentRepository.findByMajorAndMinor(1000, 50);
//		UserDocument foundObject = userDocumentRepository.findByAccount_IBeaconData_MajorAndAccount_IBeaconData_Minor(1000, 50);
		Assertions.assertThat(foundObject).isNotNull();
	}

	@Test
	public void findAllUserDociumentsReturnsAtLeastOne() {
		List<UserDocument> foundObject = userDocumentRepository.findAll();
		Assertions.assertThat(foundObject.size()).isGreaterThan(0);
	}

	private UserDocument createNewUserDocument() {
		String userId ="magulla@fb.com";
		String userName = "magulla";
		IBeaconData ibeacon =  IBeaconData.withMajorMinorPair( 1000, 50,userId);
		Account account = new Account( userName , ibeacon );
		UserDetails userDetails = new UserDetails("Vad", "Mishch", "vad@mishch.com");
		UserDocument userDocument = new UserDocument(userId, account, userDetails);
		return userDocument;
	}
}
