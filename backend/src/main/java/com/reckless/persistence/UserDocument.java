package com.reckless.persistence;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import com.reckless.account.Account;
import com.reckless.user.details.UserDetails;

public class UserDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    public final String userId;
    public final Account account;
    public final UserDetails userDetails;

    public UserDocument(String userId, Account account, UserDetails userDetails) {
        this.userId = userId;
        this.account = account;
        this.userDetails = userDetails;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDocument other = (UserDocument) obj;
        if (account == null) {
            if (other.account != null)
                return false;
        } else if (!account.equals(other.account))
            return false;
        if (userDetails == null) {
            if (other.userDetails != null)
                return false;
        } else if (!userDetails.equals(other.userDetails))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UserDocument [userId=" + userId + ", account=" + account
                + ", userDetails=" + userDetails + "]";
    }


}
