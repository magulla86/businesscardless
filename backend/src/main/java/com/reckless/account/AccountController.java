package com.reckless.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {


    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public Account getAccountForUserName(@RequestParam(value = "userName") String userName) {
        throw new UnsupportedOperationException("method is not implemented");
    }

}
