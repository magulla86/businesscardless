package com.reckless.account;

import java.io.Serializable;
import java.util.Objects;

import com.reckless.ibeacon.IBeaconData;

public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    public final String userName;
    public final IBeaconData iBeaconData;

    public Account(String userName, IBeaconData iBeaconData) {
        this.userName = userName;
        this.iBeaconData = iBeaconData;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, iBeaconData);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Account other = (Account) obj;
        if (iBeaconData == null) {
            if (other.iBeaconData != null)
                return false;
        } else if (!iBeaconData.equals(other.iBeaconData))
            return false;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Account [userName=" + userName + ", ibeacon=" + iBeaconData + "]";
    }
}
